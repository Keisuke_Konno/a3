import javax.management.RuntimeMBeanException;
import java.util.LinkedList;
import java.util.Stack;
import java.util.StringTokenizer;

public class LongStack {

   private LinkedList <Long> stack;
   public LinkedList<Long> getStack(){
      return this.stack;
   }
   public static void main (String[] argum) {
      // TODO!!! Your tests here!
   }

   LongStack() {
      // TODO!!! Your constructor here!
      this.stack = new LinkedList <Long>();
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      LongStack clone = new LongStack();
      try{
         for(int i=this.stack.size()-1; i>=0; i--){
            clone.push(this.stack.get(i));
         }
         return clone; // TODO!!! Your code here!
      }catch (Exception e){
         throw new CloneNotSupportedException("No clone"+e);
      }
   }

   public boolean stEmpty() {
      return this.stack.isEmpty(); // TODO!!! Your code here!
   }

   public void push (long a) {
      // TODO!!! Your code here!
      this.stack.push(a);
   }

   public long pop() {
      if(this.stack.get(0)==null){
         throw new RuntimeException("Empty stack");
      }
      return this.stack.pop(); // TODO!!! Your code here!
   } // pop

   public void op (String s) {
      // TODO!!!
      if(this.stack.size() < 2){
         throw new RuntimeException("Stack should contain 2 elements at least:"+s);
      }else {
         long tmp = 0;
         // make it to switch
         long a = this.stack.pop();
         long b = this.stack.pop();
         switch (s) {
            case "+":
               tmp = b + a;
               this.stack.push(tmp);
               break;
            case "-":
               tmp = b - a;
               this.stack.push(tmp);
               break;
            case "*":
               tmp = b * a;
               this.stack.push(tmp);
               break;
            case "/":
               tmp = b / a;
               this.stack.push(tmp);
               break;
            default:
               throw new RuntimeException("Input error:" + s);
         }
      }

   }
  
   public long tos() {
      if(this.stack.get(0) == null){
         throw new RuntimeException("Stack empty");
      }
      return this.stack.getFirst(); // TODO!!! Your code here!
   }

   //Reference: Idea is given by Valentine
   @Override
   public boolean equals (Object o) {
      if(o instanceof LongStack){
         return ((LongStack)o).getStack().equals(this.stack);
      }else{
         throw new RuntimeException("Not Stack"+o);
      } // TODO!!! Your code here!
   }

   @Override
   public String toString() {
      StringBuilder tmp = new StringBuilder();
      for(int i=this.stack.size()-1; i>=0; i--){
         tmp.append(this.stack.get(i));
      }

      return tmp.toString(); // TODO!!! Your code here!
   }

   public static long interpret (String pol) {
      //Reference
      // https://stackoverflow.com/questions/14100522/reverse-polish-notation-java

      //Exception empty input
      if(pol.isEmpty()){
         throw new RuntimeException("Empty input:"+pol);
      }

      long number1;
      long number2;
      pol = pol.replace("\t","");

      Stack<Long> stack = new Stack<Long>();
      String s1;
      StringTokenizer str = new StringTokenizer(pol," ");

      int ccOp=0;
      int ccNum=0;

      while (str.hasMoreElements())
      {
         s1 = (String) str.nextElement();
         //System.out.println(s1);

         if(s1.equals("+") || s1.equals("-") || s1.equals("*") || s1.equals("/"))
         {
            ccOp++;

            //Exception not enough nums
            if(ccOp > ccNum  && ccNum < 2) throw new RuntimeException("Too many op:"+s1+" in "+pol);
            switch (s1)
            {
               case "+":
                  number1 = stack.pop();
                  System.out.println("N1:"+number1);
                  number2 = stack.pop();
                  System.out.println("N2:"+number2);
                  System.out.println(number1 + " + " + number2);
                  stack.push(number2 + number1);
                  break;
               case "-":
                  number1 = stack.pop();
                  number2 = stack.pop();
                  System.out.println("N1:"+number1);
                  System.out.println("N2:"+number2);
                  System.out.println(number2 +" - "+ number1);
                  stack.push(number2 - number1);
                  break;
               case "/":
                  number1 = stack.pop();
                  number2 = stack.pop();
                  System.out.println("N1:"+number1);
                  System.out.println("N2:"+number2);
                  System.out.println(number1 +" / "+ number2);
                  stack.push(number2 / number1);
                  break;
               case "*":
                  number1 = stack.pop();
                  number2 = stack.pop();
                  System.out.println("N1:"+number1);
                  System.out.println("N2:"+number2);
                  System.out.println(number1 +" * "+ number2);
                  stack.push(number2 * number1);
                  break;

            }
         }
         else {
            //Used regex to check input
            boolean isNumeric =  s1.matches("[+-]?\\d*(\\.\\d+)?");
            if(isNumeric){
               stack.push(Long.parseLong(s1));
               ccNum++;
            }else{
               throw new RuntimeException("Illegal input"+s1+" in "+pol);
            }
         }

      }
      //Exception too many nums
      if(stack.size()==2){
         throw new RuntimeException("Too many nums:"+pol);
      }
      System.out.println(stack);
      return stack.pop(); // TODO!!! Your code here!
   }

}

